using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        //TODO
        //Add Error handles
        //Help Menu
        //First Time Startup Menu
        //List quantities on the sales log
        //More Themes

        public static int DarkTheme = 0;
        public static List<Stock> stocklist = new List<Stock>();
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();

            if (passwordgate())
            {
                Splash();
                Load();
                Menu();
            }
        }

        public struct CartValue
        {
            public string item;
            public int amount;
            public float price;
        }

        public static List<CartValue> cart = new List<CartValue>();

        public static int Choice = 0;

        static bool passwordgate()
        {
            if (!File.Exists(@"pass.txt"))
            {
                Password Newpin = new Password();

                for (bool i = false; i == false;) {
                    Console.Clear();
                    Console.WriteLine("Please enter number pin password (Dont forget this)");
                    try
                    {
                        Newpin.Passw = long.Parse(Console.ReadLine());
                        i = true;
                    }
                    catch (Exception e)
                    {
                        Console.Clear();
                        Console.WriteLine("The pin needs to be numbers");
                        Thread.Sleep(1250);
                    }
                }
                StreamWriter file = new System.IO.StreamWriter(@"pass.txt");

                file.Write(Newpin.Encrtypt());
                file.Close();
            }
            else
            {
                StreamReader file = new System.IO.StreamReader(@"pass.txt");

                Password apass = new Password();
                apass.Passw = int.Parse(file.ReadLine());
                apass.Passw = apass.Decrypt();
                string guess = "";

                while (apass.Passw.ToString() != guess) {
                    Console.Clear();
                    Console.WriteLine("What is your password?");
                    guess = Console.ReadLine();
                }
                file.Close();
            }
            Console.Clear();
            return true;
        }

        static void Splash()
        {
            Console.BackgroundColor = ConsoleColor.White;
            int waittime = 200;
            System.Threading.Thread.Sleep(waittime);
            Console.Beep(440, 200);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(@" ______  ______  ______  ______  __  __   ______    ");
            Console.WriteLine(@"/\  ___\/\__  _\/\  __ \/\  ___\/\ \/ /  /\  ___\   ");
            Console.WriteLine(@"\ \___  \/_/\ \/\ \ \/\ \ \ \___\ \  _'-.\ \___  \  ");
            Console.WriteLine(@" \/\_____\ \ \_\ \ \_____\ \_____\ \_\ \_\\/\_____\");
            Console.WriteLine(@"  \/_____/  \/_/  \/_____/\/_____/\/_/\/_/ \/_____/");
            System.Threading.Thread.Sleep(waittime);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine(@"____________________________________________________");
            System.Threading.Thread.Sleep(waittime);
            Console.WriteLine(@"____________________________________________________");

            Console.WriteLine(@"");
            System.Threading.Thread.Sleep(waittime);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("B");
            Console.Beep(440, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("y ");
            Console.Beep(495, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("A");
            Console.Beep(550, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("f");
            Console.Beep(660, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("i");
            Console.Beep(733, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Write("t");
            Console.Beep(825, 200);
            System.Threading.Thread.Sleep(waittime);
            Console.Beep(880, 200);
        }

        static void ChangeTheme()
        {
            if (DarkTheme == 0)
            {
                DarkTheme = 1;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (DarkTheme == 2)
            {
                DarkTheme = 0;
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.ForegroundColor = ConsoleColor.Magenta;
            }
            else if (DarkTheme == 1)
            {
                DarkTheme = 2;
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
            }
            Console.Clear();
            Menu();
        }

        static void Menu()
        {
            Choice = 0;

            var decided = false;
            while (!decided) {
                Console.Clear();
                Console.WriteLine("Select from the following menu:");
                Console.WriteLine("");
                DrawWithSelect(" Make A Sale", 0, Choice);
                DrawWithSelect(" Check sales", 1, Choice);
                DrawWithSelect(" Manage stock", 2, Choice);
                Console.WriteLine("");
                DrawWithSelect(" Change Theme", 3, Choice);
                Console.WriteLine("");
                DrawWithSelect(" Exit", 4, Choice);

                var key = Console.ReadKey().Key;
                if (key == ConsoleKey.UpArrow) Choice -= 1;
                if (key == ConsoleKey.DownArrow) Choice += 1;
                if (Choice < 0) Choice = 0;
                if (Choice > 4) Choice = 4;

                if (key == ConsoleKey.Enter)
                {
                    if (Choice == 4)
                    {
                        Console.Beep(440, 200);
                        return;
                    }
                    else if (Choice == 2)
                    {
                        Console.Beep(440, 200);
                        StockManager();
                        return;
                    }
                    else if (Choice == 0)
                    {
                        Console.Beep(440, 200);
                        MakeASale();
                        return;
                    }
                    else if (Choice == 1)
                    {
                        Console.Beep(440, 200);
                        ShowSales();
                        return;
                    }
                    else if (Choice == 3)
                    {
                        ChangeTheme();
                    }
                    decided = true;
                }
            }
        }

        static void StockManager()
        {

            Choice = 0;
            var decided = false;

            while(decided == false) {
                Console.Clear();
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("");
                DrawWithSelect(" Add a new item", 0, Choice);
                DrawWithSelect(" Edit an existing item", 1, Choice);
                DrawWithSelect(" View list of items", 2, Choice);
                DrawWithSelect(" Remove an existing item", 3, Choice);
                DrawWithSelect(" Delete all saved stocks", 4, Choice);
                Console.WriteLine("");
                DrawWithSelect(" Back to main menu", 5, Choice);


                var key = Console.ReadKey().Key;
            if (key == ConsoleKey.UpArrow) Choice -= 1;
            if (key == ConsoleKey.DownArrow) Choice += 1;
            if (Choice < 0) Choice = 0;
            if (Choice > 5) Choice = 5;

                //Finish Off Menu
                if (key == ConsoleKey.Enter)
                {
                    if (Choice == 5)
                    {
                        Console.Beep(440, 200);
                        Menu();
                        return;
                    }
                    if (Choice == 0)
                    {
                        Console.Beep(440, 200);
                        Console.Clear();
                        AddStock();
                        return;
                    }
                    if (Choice == 1)
                    {
                        Console.Beep(440, 200);
                        Console.Clear();
                        EditStock();
                        return;
                    }
                    if (Choice == 2)
                    {
                        Console.Beep(440, 200);
                        Console.Clear();
                        DisplayStocks();
                        Console.ReadKey();
                        Menu();
                        return;
                    }
                    if (Choice == 3)
                    {
                        Console.Beep(440, 200);
                        RemoveStock();
                        return;
                    }
                    if (Choice == 4)
                    {
                        Console.Beep(440, 200);
                        clearsaves();
                        return;
                    }
                    decided = true;
                }
            }
        }

        static void DrawWithSelect(string line, int OrderNumb, int CurrentChoice)
        {
            if (OrderNumb == CurrentChoice)
            {
                Console.WriteLine(">" + line);
            }
            else
            {
                Console.WriteLine(" " + line);
            }
        }

        static void Load()
        {
            if (!Directory.Exists(@"Save\"))
            {
                DirectoryInfo di = Directory.CreateDirectory(@"Save\");
            }
            stocklist.Clear();
            string[] filePaths = Directory.GetFiles(@"Save\", "*.txt", SearchOption.AllDirectories);
            foreach (string filee in filePaths)
            {
                using (var streamReader = new StreamReader(filee, Encoding.UTF8))
                {
                    try
                    {
                        stocklist.Add(new Stock()
                        {
                            Name = streamReader.ReadLine(),
                            Price = float.Parse(streamReader.ReadLine())
                        });
                    } catch (Exception e)
                    {}
                }
            }
        }

        static void clearsaves()
        {
            Console.Clear();
            Console.Write("Are You Sure? (y/n)");
            var input = Console.ReadLine();
            if (input == "y")
            {
                Console.Beep(440, 200);
                stocklist.Clear();
                Array.ForEach(Directory.GetFiles(@"Save\"),
              delegate (string path) { File.Delete(path); });
            }
            Menu();
        }

        static void RemoveStock()
        {
            Console.Clear();
            Console.WriteLine("Choose A Stock To Delete");
            DisplayStocks();
            Console.Write("Stock Name: ");
            var namee = Console.ReadLine();
            if (File.Exists(@"Save\" + namee + ".txt"))
            {
                File.Delete(@"Save\" + namee + ".txt");
                stocklist.Clear();
                Load();
                Menu();
            }
            else
            {
                Menu();
            }
        }

        static void EditStock()
        {
            if (stocklist.Count > 0)
            {
                Console.Clear();
                Console.WriteLine("Choose A Stock To Edit");
                DisplayStocks();
                Console.Write("Stock Name: ");
                var namee = Console.ReadLine();
                if (File.Exists(@"Save\" + namee + ".txt"))
                {
                    File.Delete(@"Save\" + namee + ".txt");
                    string path = @"Save\" + namee + ".txt";
                    if (!File.Exists(path))
                    {
                        TextWriter tw = new StreamWriter(path);
                        tw.WriteLine(namee);
                        var dec = false;
                        while (!dec)
                        {
                            Console.Clear();
                            Console.WriteLine("Enter New Price:");
                            try
                            {
                                tw.WriteLine(float.Parse(Console.ReadLine()));
                                dec = true;
                            }
                            catch (Exception e) { }
                        }
                        tw.Close();
                        Load();
                        Menu();
                    }
                }
                else
                {
                    EditStock();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("You have no stocks to edit");
                Console.ReadLine();
                StockManager();
            }
        }

        static void AddStock()
        {
            var name = "";
            var price = "";
            bool isnumber = false;
            Console.WriteLine("Please submit a name");
            name = Console.ReadLine();
            float p = 0f;
            while (isnumber == false)
            {

                Console.WriteLine("Please submit a price");
                price = Console.ReadLine();
                try
                {
                    p = float.Parse(price);
                    isnumber = true;
                }
                catch (Exception e)
                {
                    isnumber = false;
                }
            }
            CreateStock(name, p);
            Menu();
        }

        static void save(string sname, string sprice)
        {
            string path = @"Save\" + sname + ".txt";
            if (!File.Exists(path))
            {
                TextWriter tw = new StreamWriter(path);
                tw.WriteLine(sname);
                tw.WriteLine(sprice);
                tw.Close();
            }
            else if (File.Exists(path))
            {
                Console.WriteLine("An Item With That Name Already Exists");
                Console.ReadKey();
            }
        }

        static void CreateStock(string name, float price)
        {
            stocklist.Add(new Stock()
            {
                Name = name,
                Price = price
            });
            save(name, price.ToString());
        }

        static void DisplayStocks()
        {
            Console.WriteLine("Stock List:");
            foreach (Stock st in stocklist)
            {
                Console.WriteLine("");
                Console.WriteLine("  Name: " + st.Name);
                Console.WriteLine("  Price: {0:C2}", st.Price);
            }
        }

        static void MakeASale()
        {
            var Decided = false;
            Choice = 0;

            while (!Decided)
            {
                Console.Clear();

                Console.WriteLine("Welcome to the sales screen! Add your items and then head to the checkout");
                Console.WriteLine("");
                DrawWithSelect(" Add to cart", 0, Choice);
                DrawWithSelect(" Remove from cart", 1, Choice);
                Console.WriteLine("");
                DrawWithSelect(" Finalise sale", 2, Choice);
                Console.WriteLine("");
                DrawWithSelect(" Main Menu", 3, Choice);

                Console.WriteLine("------------------- Cart -------------------");
                foreach (CartValue cv in cart)
                {
                    Console.WriteLine("");
                    Console.Write("-  " + cv.item + " x" + cv.amount + " ");
                    Console.Write("  ({0:C2} each)", cv.price);
                    Console.WriteLine("");
                }
                Console.WriteLine("");
                float totalprice = 0;
                foreach (CartValue cv in cart)
                {
                    totalprice += cv.amount * cv.price;
                }
                Console.Write("Total: {0:C2}", totalprice);

                var key = Console.ReadKey().Key;
                if (key == ConsoleKey.UpArrow) Choice -= 1;
                if (key == ConsoleKey.DownArrow) Choice += 1;
                if (Choice < 0) Choice = 0;
                if (Choice > 3) Choice = 3;

                if (key == ConsoleKey.Enter)
                {
                    if (Choice == 3) Menu();
                    if (Choice == 0) AddToCart();
                    if (Choice == 2) FinalizeSale(totalprice);
                    if (Choice == 1) Removefromcart();

                    Decided = true;
                }
            }
        }

        static void AddToCart()
        {
            Console.Clear();
            if(stocklist.Count == 0)
            {
                Console.WriteLine("No stocks exist, please add stocks via the stocks manager");
                Console.ReadKey();
                Menu();
            }
            bool dec = false;
            
            while (!dec)
            {
                Console.Clear();
                Console.WriteLine("Choose an item to add to the list");
                DisplayStocks();
                Console.Write("Stock Name: ");
                var namee = Console.ReadLine();

            
                string file = @"Save\" + namee + ".txt";
                CartValue cv = new CartValue();

                if (File.Exists(file))
                {
                    dec = true;
                    cv.item = namee;
                    using (var streamReader = new StreamReader(file, Encoding.UTF8))
                    {
                        cv.item = streamReader.ReadLine();
                        cv.price = float.Parse(streamReader.ReadLine());
                    }

                    bool i = false;
                    string input = "";
                    while (!i) {
                        Console.Clear();
                        Console.WriteLine("How many would you like to purchase?");

                        input = Console.ReadLine();

                        if (input == "") input = "0";

                        try
                        {
                            cv.amount = int.Parse(input);
                            i = true;
                        }
                        catch(Exception e)
                        {
                        }

                    }
                    if (int.Parse(input) >= 0)
                    {
                        var exists = false;
                        var equivitem = new CartValue();
                        foreach (CartValue cartval in cart)
                        {
                            if (cv.item == cartval.item)
                            {
                                equivitem = cartval;
                                exists = true;
                            }
                        }
                        if (exists)
                        {
                            cv.amount = cv.amount + equivitem.amount;
                            cart.Remove(equivitem);
                        }
                        cart.Add(cv);
                    }
                    MakeASale();
                }
            }
        }

        static void Removefromcart()
        {
            if (cart.Count > 0)
            {
                var Decided = false;
                Choice = 0;

                while (!Decided)
                {
                    Console.Clear();
                    Console.WriteLine("Choose the item you would like to remove from cart");

                    for (int i = 0; i < cart.Count; i++)
                    {
                        Console.WriteLine("");
                        DrawWithSelect(cart[i].item, i, Choice);
                        DrawWithSelect("x" + cart[i].amount.ToString(), i, Choice);
                    }

                    var key = Console.ReadKey().Key;

                    if (key == ConsoleKey.UpArrow) Choice -= 1;
                    if (key == ConsoleKey.DownArrow) Choice += 1;
                    if (Choice < 0) Choice = 0;
                    if (Choice > cart.Count - 1) Choice = cart.Count - 1;

                    if (key == ConsoleKey.Enter)
                    {
                        Decided = true;
                    }
                }

                var decided2 = false;
                var amount = cart[Choice].amount;

                while (!decided2)
                {
                    Console.Clear();
                    Console.WriteLine("How many would you like to remove?");
                    Console.WriteLine("");
                    Console.WriteLine(numbCursor(amount, cart[Choice].amount));

                    var key = Console.ReadKey().Key;
                    if (key == ConsoleKey.LeftArrow) amount -= 1;
                    if (key == ConsoleKey.RightArrow) amount += 1;
                    if (amount < 0) amount = 0;
                    if (amount > cart[Choice].amount) amount = cart[Choice].amount;

                    if (key == ConsoleKey.Enter)
                    {
                        if (amount == 0) return;

                        if (amount == cart[Choice].amount)
                        {
                            cart.Remove(cart[Choice]);
                        }
                        else
                        {
                            CartValue newitem = new CartValue();
                            newitem = cart[Choice];
                            cart.Remove(cart[Choice]);

                            newitem.amount = newitem.amount - amount;

                            cart.Add(newitem);
                        }
                        decided2 = true;
                    }
                }
                MakeASale();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Your cart is already empty");
                Console.ReadLine();
                MakeASale();
            }
        }

        static string numbCursor(int number, int maxAmount)
        {
            if (number == maxAmount) return "<" + number;
            if (number == 0) return " " + number + ">";
            return "<" + number + ">";
        }

        static void FinalizeSale(float price)
        {
            Choice = 0;
            var decided = false;
            while (!decided)
            {
                Console.Clear();
                Console.WriteLine("Would you like to finalize the following sale of:");

                foreach (CartValue cv in cart)
                {
                    Console.WriteLine("");
                    Console.Write("-  " + cv.item + " x" + cv.amount + " ");
                    Console.Write("  ({0:C2} each)", cv.price);
                    Console.WriteLine("");
                }
                Console.WriteLine("Total: {0:C2}", price);
                Console.WriteLine("");

                DrawWithSelect(" Yes", 0, Choice);
                DrawWithSelect(" No", 1, Choice);

                var key = Console.ReadKey().Key;
                if (key == ConsoleKey.UpArrow) Choice -= 1;
                if (key == ConsoleKey.DownArrow) Choice += 1;
                if (Choice < 0) Choice = 0;
                if (Choice > 1) Choice = 1;

                if (key == ConsoleKey.Enter)
                {
                    if (Choice == 1) Menu();

                    if (Choice == 0)
                    {
                        float paid = 0.069f;
                        Console.Clear();

                        while (paid == 0.069f)
                        {
                            Console.Clear();
                            Console.WriteLine("How much was paid? {0:C2} is owed", price);
                            try
                            {
                                paid = float.Parse(Console.ReadLine());
                            }
                            catch (Exception e)
                            {}
                        }

                        Console.Clear();
                        
                        if (paid < price)
                        {
                            Console.WriteLine("Amount paid was lower than amount owed, returning to cart");
                            Console.ReadKey();
                            MakeASale();
                        }
                        else
                        {
                            if (paid == price) Console.WriteLine("Exact cost was paid");
                            if (paid > price) Console.WriteLine("Amount paid was higher than amount owed, {0:C2} in change", paid - price);

                            Console.ReadKey();

                            string path = @"SaleLog.txt";
                            TextWriter tw = new StreamWriter(path, true);

                            string list = "";

                            foreach (CartValue cv in cart)
                            {
                                list = list + cv.item + "x ($" + cv.amount + " each)" + ", ";
                            }
                            tw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy") + ":" + DateTime.Now.ToString("h:mm:ss tt") + " - " + list + "  Total Price: {0:C2}", price);
                            tw.Close();

                            cart.Clear();
                            Menu();
                        }
                    }
                    decided = true;
                }
            }
        }

        static void ShowSales()
        {
            Console.Clear();
            Console.WriteLine("Sales:");

            int counter = 0;
            string line;

            // Reads the file and display it line by line  
            if (File.Exists(@"SaleLog.txt")) {
                StreamReader file = new System.IO.StreamReader(@"SaleLog.txt");
                while ((line = file.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    counter++;
                }
                file.Close();
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("You have no sale history");
            }
            Console.ReadKey();
            Menu();
        }
    }

    class Stock
    {
        public string Name { get; set; }
        public float Price { get; set; }
    }

    class Password
    {
        public const int key = 4811;
        public long Passw { get; set; }
        public long Encrtypt()
        {
            return (Passw + 1) * key + key;
        }

        public long Decrypt()
        {
            return (Passw - key) / key - 1;
        }
    }
}
